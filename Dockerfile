# -- Versioning --

FROM ruby:2.7

# -- Ruby --

ENV PATH="/usr/local/ruby/bin:${PATH}"

ENV GEM_HOME="/usr/local/bundle"
ENV PATH $GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH

ENV DEBIAN_FRONTEND=noninteractive

# -- Build Deps --
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get update -qq && apt-get install -qq --no-install-recommends nodejs && \
    apt-get upgrade -qq && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    npm install -g yarn@1

RUN apt update && \
    apt -y install python2

RUN npm install -g yarn gulp && \
    gem install bundler foreman

# -- Copy --

WORKDIR /app

# We copy the depfiles and run those first, so that we only have to
# invalidate the layer cache if these files change, not just any file
COPY Gemfile Gemfile.lock package.json yarn.lock ./

# -- Install Pre-Reqs --

RUN bundle config --global silence_root_warning 1 && \
  bundle config --local with 'development test' &&\
  bundle install && \
  bundle update

RUN yarn install --force

# -- Copy Everything --

# This is the last step, because we don't want file changes to invalidate early layer caches
COPY . .

ENTRYPOINT ["./docker-entrypoint.sh"]
