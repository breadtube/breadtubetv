#!/bin/bash

set -e

rails db:create db:migrate

exec "$@"
